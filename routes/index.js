var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});
router.get("/aaa", function (req, res, next) {
  res.send("respond with a aaaa");
});
module.exports = router;
