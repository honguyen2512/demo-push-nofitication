let express = require("express");
const { createServer } = require("http");
let path = require("path");
let cookieParser = require("cookie-parser");
let logger = require("morgan");
let createError = require("http-errors");
const cors = require("cors");

const app = express();
const httpServer = createServer(app);
let indexRouter = require("./routes/index");
let usersRouter = require("./routes/users");
let NotificationRouter = require("./routes/nofitication");
let apiSocKet = require("./routes/api");

app.use(cors()); // cho phép tất cả FE truy cập vào API của BE
// tạo server localhost với port 8080 => localhost:8080
app.listen(3003);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/nofitication", NotificationRouter);
app.use("/api", apiSocKet);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
